package com.nicoleapp.githubuser.root;

import com.nicoleapp.githubuser.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
})
public interface ApplicationComponent {

    void inject(MainActivity target);
}
