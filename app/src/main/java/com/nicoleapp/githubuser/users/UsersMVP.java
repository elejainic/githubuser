package com.nicoleapp.githubuser.users;

import io.reactivex.Observable;

public interface UsersMVP {

    interface View{
        void updateData(ViewModel viewModel);

        void showSnackbar(String s);
    }

    interface Presenter{
        void loadData();

        void rxJavaUnsuscribe();

        void setView(UsersMVP.View view);
    }

    interface Model {
        Observable<ViewModel> result();
    }

}
