package com.nicoleapp.githubuser.users;

public class ViewModel {

    private String name;
    private String lasrName;

    public ViewModel(String name, String lasrName) {
        this.name = name;
        this.lasrName = lasrName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLasrName() {
        return lasrName;
    }

    public void setLasrName(String lasrName) {
        this.lasrName = lasrName;
    }
}
