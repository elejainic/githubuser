package com.nicoleapp.githubuser.users;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class UsersPresenter implements UsersMVP.Presenter {

    private UsersMVP.View view;
    private UsersMVP.Model model;

    private Disposable subscription = null;


    public UsersPresenter(UsersMVP.Model model){
        this.model = model;
    }


    @Override
    public void loadData() {

        subscription = model.result()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ViewModel>() {
                    @Override
                    public void onNext(ViewModel viewModel) {
                        if(view != null){
                            view.updateData(viewModel);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view != null){
                            view.showSnackbar("Error downloading the user...");
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (view != null){
                            view.showSnackbar("Information downloaded  correctly");
                        }
                    }
                });

    }

    @Override
    public void rxJavaUnsuscribe() {
        if (subscription!= null && !subscription.isDisposed()){
            subscription.dispose();
        }
    }

    @Override
    public void setView(UsersMVP.View view) {
        this.view = view;
    }
}
